import React, { useEffect, useState } from 'react'
import './App.css'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import CardActions from '@material-ui/core/CardActions'
import TaskCreator from './TaskCreator'
import axios from 'axios'
import Swal from 'sweetalert2'
import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'

const makeGridItem = (id, component) => (<Grid item xs={12} sm={6} key={id}>
  {component}
</Grid>)

function App () {
  const [tasks, setTasks] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const loadTasks = () => {
    setIsLoading(true)
    axios.get('http://127.0.0.1:8000/api/tasks/')
      .then(response => {
        setIsLoading(false)
        setTasks(response.data)
      })
      .catch(err => {
        setIsLoading(false)
        Swal.fire('Упс', 'Ошибка при получении данных с сервера',
          'error')
      })
  }
  useEffect(loadTasks, [])

  const deleteTask = (id) => {
    axios.delete('http://127.0.0.1:8000/api/tasks/' + id).then(loadTasks)
  }

  const createTask = (task) => {
    axios.post('http://127.0.0.1:8000/api/tasks/', task).then(loadTasks)
  }

  return <>
    <Backdrop open={isLoading} style={{ zIndex: 3000 }}>
      <CircularProgress color="secondary"/>
    </Backdrop>
    <AppBar position="static">
      <Toolbar>
        <Container>
          <Typography variant="h4">
            Tasks List
          </Typography>
        </Container>
      </Toolbar>
    </AppBar>
    <Box component={Container} my={2}>
      <Grid container spacing={1}>
        {tasks.map(t => makeGridItem(t.id, <Card elevation={3}>
          <CardContent>
            <Typography variant='h6'>
              {t.name}
            </Typography>
            <Typography color='textSecondary'>
              {t.description}
            </Typography>
          </CardContent>
          <CardActions>
            <Button variant='outlined' color='secondary'
                    onClick={() => deleteTask(t.id)}>
              Удалить
            </Button>
          </CardActions>
        </Card>))}
        {makeGridItem('creator', <TaskCreator createTask={createTask}/>)}
      </Grid>
    </Box>
  </>
}

export default App
