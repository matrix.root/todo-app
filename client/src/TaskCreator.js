import React, { useState } from 'react'
import TextField from '@material-ui/core/TextField'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import CardActions from '@material-ui/core/CardActions'

export default function TaskCreator ({ createTask }) {
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')

  return <Card elevation={3}>
    <CardContent>
      <TextField label="Название" value={name}
                 onChange={event => setName(event.target.value)} fullWidth/>
      <TextField label="Описание" value={description}
                 onChange={event => setDescription(event.target.value)}
                 fullWidth/>
    </CardContent>
    <CardActions>
      <Button variant='outlined' color='primary'
              onClick={() => createTask({
                name,
                description,
              })}>
        Создать
      </Button>
    </CardActions>
  </Card>
}