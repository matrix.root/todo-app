from rest_framework.routers import SimpleRouter

from todo_core.views import TaskViewSet

router = SimpleRouter()
router.register('tasks', TaskViewSet)
