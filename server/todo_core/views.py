from rest_framework.viewsets import ModelViewSet

from todo_core.models import Task
from todo_core.serializers import TaskSerializer


class TaskViewSet(ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
